﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProjectWatch.EF.Models;
using ProjectWatch.Filters;

namespace ProjectWatch.Tests
{
    [TestClass]
    public class MigrateUsersTest
    {
        [TestMethod]
        [InitializeSimpleMembership]
        public void MigrateUsers()
        {
            using (var db = new SAV_ProjectWatchContext())
            {
                var users = db.Users.ToList();

                foreach (var userProfile in from user in users
                    let userProfile = db.UserProfiles.SingleOrDefault(x => x.UserName == user.UserName)
                    where userProfile == null
                    select new UserProfile {UserName = user.UserName})
                {
                    db.UserProfiles.Add(userProfile);
                }

                db.SaveChanges();
            }
        }
    }
}
