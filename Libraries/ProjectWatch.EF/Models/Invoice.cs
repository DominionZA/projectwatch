using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public sealed class Invoice
    {
        public Invoice()
        {
            InvoicePayments = new List<InvoicePayment>();
            LineItems = new List<LineItem>();
            Timesheets = new List<Timesheet>();
        }

        public int InvoicesId { get; set; }
        public Guid? Guid { get; set; }
        public int fkUsersID { get; set; }
        public int fkAccountsID { get; set; }
        public System.DateTime DateInserted { get; set; }
        public System.DateTime InvoiceDate { get; set; }
        public DateTime? DueDate { get; set; }
        public double VATPercent { get; set; }
        public string InvoiceNo { get; set; }
        public string Terms { get; set; }
        public string Notes { get; set; }
        public int? Status { get; set; }
        public bool? Active { get; set; }
        public string ClientDetails { get; set; }
        public string OrderNo { get; set; }
        public Account Account { get; set; }
        public ICollection<InvoicePayment> InvoicePayments { get; set; }
        public User User { get; set; }
        public ICollection<LineItem> LineItems { get; set; }
        public ICollection<Timesheet> Timesheets { get; set; }
    }
}
