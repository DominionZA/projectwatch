using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public sealed class ProjectActivity
    {
        public ProjectActivity()
        {
            Timesheets = new List<Timesheet>();
        }

        public int ProjectActivitiesId { get; set; }
        public string ProjectActivityName { get; set; }
        public int fkProjectsID { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateTimeInserted { get; set; }
        public decimal? BillRate { get; set; }
        public Project Project { get; set; }
        public ICollection<Timesheet> Timesheets { get; set; }
    }
}
