using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public sealed class Project
    {
        public Project()
        {
            ProjectActivities = new List<ProjectActivity>();
        }

        public int ProjectsId { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public int fkUsersID_Client { get; set; }
        public bool? Active { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal? Budget { get; set; }
        public double? BudgetHours { get; set; }
        public ICollection<ProjectActivity> ProjectActivities { get; set; }
    }
}
