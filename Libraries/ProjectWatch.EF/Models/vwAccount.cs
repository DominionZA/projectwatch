using System;

namespace ProjectWatch.EF.Models
{
    public class VwAccount
    {
        public int AccountsId { get; set; }
        public string AccountName { get; set; }
        public string AccountSerial { get; set; }
        public DateTime? DateCreated { get; set; }
        public string AccountLogoPath { get; set; }
        public bool Active { get; set; }
        public string InvoiceNoPrefix { get; set; }
        public string InvoiceNoSuffix { get; set; }
        public int InvoiceNoSeed { get; set; }
        public int InvoiceNoLength { get; set; }
        public double InvoiceVatPercent { get; set; }
        public string PhysAddress1 { get; set; }
        public string PhysAddress2 { get; set; }
        public string PhysCity { get; set; }
        public string PhysProvince { get; set; }
        public string PhysCountry { get; set; }
        public string PhysPostalCode { get; set; }
        public string DayPhone { get; set; }
        public string MobilePhone { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public string EmailSalutation { get; set; }
        public string DefaultTerms { get; set; }
        public string DefaultNotes { get; set; }
        public string VatNo { get; set; }
        public string CkNo { get; set; }
        public string HomePage { get; set; }
        public int? fkPackagesID { get; set; }
        public string Package { get; set; }
    }
}
