using System;

namespace ProjectWatch.EF.Models
{
    public class C__ProjectActivitiesInProjects
    {
        public int ProjectActivitiesInProjectsId { get; set; }
        public int fkProjectsID { get; set; }
        public int fkProjectActivitiesID { get; set; }
        public decimal BillRate { get; set; }
        public DateTime? DateTimeInserted { get; set; }
    }
}
