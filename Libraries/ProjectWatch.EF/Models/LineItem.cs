using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public partial class LineItem
    {
        public int LineItemsId { get; set; }
        public int fkInvoicesID { get; set; }
        public int? fkItemsID { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public decimal? ItemPriceExcl { get; set; }
        public decimal? ItemCostExcl { get; set; }
        public decimal? ItemDiscount { get; set; }
        public double ItemQuantity { get; set; }
        public string Measure { get; set; }
        public DateTime? DateTimeInserted { get; set; }
        public double? ItemTotalExcl { get; set; }
        public virtual Invoice Invoice { get; set; }
    }
}
