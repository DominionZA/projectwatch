using System;

namespace ProjectWatch.EF.Models
{
    public class ProjectActivities_Default
    {
        public int ProjectActivitiesDefaultId { get; set; }
        public string ProjectActivityName { get; set; }
        public int fkAccountsID { get; set; }
        public bool? Active { get; set; }
        public DateTime? DateTimeInserted { get; set; }
        public decimal? BillRate { get; set; }
    }
}
