namespace ProjectWatch.EF.Models
{
    public class ProductCategory
    {
        public int ProductCategoriesId { get; set; }
        public string Description { get; set; }
        public int fkAccountsID { get; set; }
    }
}
