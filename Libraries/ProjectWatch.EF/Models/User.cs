using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public sealed class User
    {
        public User()
        {
            Invoices = new List<Invoice>();
            Timesheets = new List<Timesheet>();
        }

        public int UsersId { get; set; }
        public string UserCode { get; set; }
        public string CompanyName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DayPhone { get; set; }
        public string HomePhone { get; set; }
        public string CellPhone { get; set; }
        public string Fax { get; set; }
        public string PhysAddress1 { get; set; }
        public string PhysAddress2 { get; set; }
        public string PhysCity { get; set; }
        public string PhysProvince { get; set; }
        public string PhysPostalCode { get; set; }
        public string PhysCountry { get; set; }
        public string PostAddress1 { get; set; }
        public string PostAddress2 { get; set; }
        public string PostCity { get; set; }
        public string PostProvince { get; set; }
        public string PostPostalCode { get; set; }
        public string PostCountry { get; set; }
        public string Email { get; set; }
        public int fkAccountsID { get; set; }
        public bool? Validated { get; set; }
        public int? AccessLevel { get; set; }
        public DateTime? DateCreated { get; set; }
        public DateTime? LastLoginDate { get; set; }
        public string Notes { get; set; }
        public bool Active { get; set; }
        public string VatNo { get; set; }
        public decimal? BillRate { get; set; }
        public decimal? CostRate { get; set; }
        public int? WorkHoursPerDay { get; set; }
        public System.Guid UserGUID { get; set; }
        public Account Account { get; set; }
        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<Timesheet> Timesheets { get; set; }
    }
}
