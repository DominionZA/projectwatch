using System;

namespace ProjectWatch.EF.Models
{
    public class VwInvoicesEx
    {
        public double InvoiceSubTotalExcl { get; set; }
        public decimal? InvoiceVatAmount { get; set; }
        public decimal? InvoiceTotalIncl { get; set; }
        public decimal? InvoicePaidToDate { get; set; }
        public decimal? InvoiceBalanceIncl { get; set; }
        public decimal? C30Days { get; set; }
        public decimal? C60Days { get; set; }
        public decimal? C90Days { get; set; }
        public decimal? C90DaysPlus { get; set; }
        public double VATPercent { get; set; }
        public int InvoicesID { get; set; }
        public int fkUsersID { get; set; }
        public int fkAccountsID { get; set; }
        public DateTime DateInserted { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime? DueDate { get; set; }
        public string InvoiceNo { get; set; }
        public string Terms { get; set; }
        public string Notes { get; set; }
        public int? Status { get; set; }
        public bool? Active { get; set; }
        public string ClientDetails { get; set; }
        public string OrderNo { get; set; }
        public string StatusDescription { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CompanyName { get; set; }
        public string VATNo { get; set; }
        public string CKNo { get; set; }
        public string DayPhone { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
    }
}
