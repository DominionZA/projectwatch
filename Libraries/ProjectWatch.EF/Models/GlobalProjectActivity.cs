using System;

namespace ProjectWatch.EF.Models
{
    public class GlobalProjectActivity
    {
        public int GlobalProjectActivitiesId { get; set; }
        public string GlobalProjectActivityName { get; set; }
        public string GlobalProjectActivityDescription { get; set; }
        public DateTime? DateTimeInserted { get; set; }
        public decimal? BillRate { get; set; }
        public bool? UseProjectBillRate { get; set; }
        public int fkAccountsID { get; set; }
    }
}
