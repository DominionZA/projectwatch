using System;

namespace ProjectWatch.EF.Models
{
    public class VwTicket
    {
        public string FromUser { get; set; }
        public string FromAccountName { get; set; }
        public string FromEmail { get; set; }
        public int? FromAccountID { get; set; }
        public string ToAccount { get; set; }
        public int TicketsID { get; set; }
        public int fkUsersID { get; set; }
        public int fkAccountsID { get; set; }
        public DateTime? DatePosted { get; set; }
        public string Subject { get; set; }
        public int? StatusCode { get; set; }
        public string TicketNo { get; set; }
        public int? fkUsersID_Operator { get; set; }
        public string StatusDescription { get; set; }
    }
}
