using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public class VwInvoiceSummaryByMonth
    {
        public int fkAccountsID { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public double? SumInvoiceSubTotalExcl { get; set; }
        public decimal? SumInvoiceVatAmount { get; set; }
        public decimal? SumInvoiceTotalIncl { get; set; }
        public decimal? SumInvoicePaidToDate { get; set; }
        public decimal? SumInvoiceBalanceIncl { get; set; }
    }
}
