using System;

namespace ProjectWatch.EF.Models
{
    public class VwTimesheetsDaily
    {
        public System.DateTime ForDate { get; set; }
        public string StaffFullName { get; set; }
        public int? StaffWorkHoursPerDay { get; set; }
        public double? Hours { get; set; }
        public double? StaffHoursLost { get; set; }
        public double? StaffCost { get; set; }
        public double? StaffBilled { get; set; }
        public double? StaffProfit { get; set; }
        public decimal? StaffProfitLost { get; set; }
        public int IsInvoiced { get; set; }
        public int FkAccountsId { get; set; }
        public int? fkInvoicesID { get; set; }
    }
}
