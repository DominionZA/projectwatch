namespace ProjectWatch.EF.Models
{
    public class TimesheetBillingAndCostingRule
    {
        public int TimesheetBillingAndCostingRulesId { get; set; }
        public int fkAccountsID { get; set; }
        public int? fkUsersID_Client { get; set; }
        public int? fkProjectsID { get; set; }
        public int? fkProjectActivitiesID { get; set; }
        public int? fkUsersID_Staff { get; set; }
        public int RuleType { get; set; }
        public decimal? Value { get; set; }
        public int ApplyRule { get; set; }
    }
}
