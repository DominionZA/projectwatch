using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public class Quote
    {
        public int QuotesId { get; set; }
        public Guid? Guid { get; set; }
        public int fkUsersID { get; set; }
        public int fkAccountsID { get; set; }
        public DateTime? DateInserted { get; set; }
        public System.DateTime QuoteDate { get; set; }
        public DateTime? QuoteValidUntil { get; set; }
        public double VATPercent { get; set; }
        public string InvoiceNo { get; set; }
        public string Notes { get; set; }
        public bool? Active { get; set; }
        public string ClientDetails { get; set; }
    }
}
