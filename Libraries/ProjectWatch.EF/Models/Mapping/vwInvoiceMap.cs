using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwInvoiceMap : EntityTypeConfiguration<VwInvoice>
    {
        public VwInvoiceMap()
        {
            // Primary Key
            HasKey(t => new { t.InvoiceSubTotalExcl, VATPercent = t.VatPercent, InvoicesID = t.InvoicesId, t.fkUsersID, t.fkAccountsID, t.DateInserted, t.InvoiceDate, t.InvoiceNo });

            // Properties
            Property(t => t.InvoicesId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.fkUsersID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkAccountsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.InvoiceNo)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.Terms)
                .HasMaxLength(1000);

            Property(t => t.Notes)
                .HasMaxLength(1000);

            Property(t => t.ClientDetails)
                .HasMaxLength(1000);

            Property(t => t.OrderNo)
                .HasMaxLength(50);

            Property(t => t.StatusDescription)
                .HasMaxLength(9);

            Property(t => t.FirstName)
                .HasMaxLength(50);

            Property(t => t.LastName)
                .HasMaxLength(50);

            Property(t => t.CompanyName)
                .HasMaxLength(100);

            Property(t => t.VatNo)
                .HasMaxLength(50);

            Property(t => t.CkNo)
                .HasMaxLength(50);

            Property(t => t.DayPhone)
                .HasMaxLength(20);

            Property(t => t.FaxNo)
                .HasMaxLength(20);

            Property(t => t.Email)
                .HasMaxLength(200);

            // Table & Column Mappings
            ToTable("vwInvoices");
            Property(t => t.InvoiceSubTotalExcl).HasColumnName("InvoiceSubTotalExcl");
            Property(t => t.InvoiceVatAmount).HasColumnName("InvoiceVATAmount");
            Property(t => t.InvoiceTotalIncl).HasColumnName("InvoiceTotalIncl");
            Property(t => t.InvoicePaidToDate).HasColumnName("InvoicePaidToDate");
            Property(t => t.InvoiceBalanceIncl).HasColumnName("InvoiceBalanceIncl");
            Property(t => t.C30Days).HasColumnName("30Days");
            Property(t => t.C60Days).HasColumnName("60Days");
            Property(t => t.C90Days).HasColumnName("90Days");
            Property(t => t.C90DaysPlus).HasColumnName("90DaysPlus");
            Property(t => t.VatPercent).HasColumnName("VATPercent");
            Property(t => t.Guid).HasColumnName("Guid");
            Property(t => t.InvoicesId).HasColumnName("InvoicesID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.DateInserted).HasColumnName("DateInserted");
            Property(t => t.InvoiceDate).HasColumnName("InvoiceDate");
            Property(t => t.DueDate).HasColumnName("DueDate");
            Property(t => t.InvoiceNo).HasColumnName("InvoiceNo");
            Property(t => t.Terms).HasColumnName("Terms");
            Property(t => t.Notes).HasColumnName("Notes");
            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.ClientDetails).HasColumnName("ClientDetails");
            Property(t => t.OrderNo).HasColumnName("OrderNo");
            Property(t => t.StatusDescription).HasColumnName("StatusDescription");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.CompanyName).HasColumnName("CompanyName");
            Property(t => t.VatNo).HasColumnName("VATNo");
            Property(t => t.CkNo).HasColumnName("CKNo");
            Property(t => t.DayPhone).HasColumnName("DayPhone");
            Property(t => t.FaxNo).HasColumnName("FaxNo");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.ReportMonth).HasColumnName("ReportMonth");
        }
    }
}
