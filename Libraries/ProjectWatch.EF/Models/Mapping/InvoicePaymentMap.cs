using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class InvoicePaymentMap : EntityTypeConfiguration<InvoicePayment>
    {
        public InvoicePaymentMap()
        {
            // Primary Key
            HasKey(t => t.InvoicePaymentsId);

            // Properties
            Property(t => t.PaymentNotes)
                .HasMaxLength(2000);

            // Table & Column Mappings
            ToTable("InvoicePayments");
            Property(t => t.InvoicePaymentsId).HasColumnName("InvoicePaymentsID");
            Property(t => t.fkInvoicesID).HasColumnName("fkInvoicesID");
            Property(t => t.PaymentDate).HasColumnName("PaymentDate");
            Property(t => t.PaymentNotes).HasColumnName("PaymentNotes");
            Property(t => t.PaymentAmount).HasColumnName("PaymentAmount");
            Property(t => t.fkPaymentMethodsID).HasColumnName("fkPaymentMethodsID");
            Property(t => t.IsReversal).HasColumnName("IsReversal");

            // Relationships
            HasRequired(t => t.Invoice)
                .WithMany(t => t.InvoicePayments)
                .HasForeignKey(d => d.fkInvoicesID);
            HasRequired(t => t.PaymentMethod)
                .WithMany(t => t.InvoicePayments)
                .HasForeignKey(d => d.fkPaymentMethodsID);

        }
    }
}
