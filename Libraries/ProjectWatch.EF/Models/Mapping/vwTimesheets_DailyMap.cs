using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwTimesheetsDailyMap : EntityTypeConfiguration<VwTimesheetsDaily>
    {
        public VwTimesheetsDailyMap()
        {
            // Primary Key
            HasKey(t => new { t.ForDate, t.IsInvoiced, fkAccountsID = t.FkAccountsId });

            // Properties
            Property(t => t.StaffFullName)
                .HasMaxLength(101);

            Property(t => t.IsInvoiced)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.FkAccountsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("vwTimesheets_Daily");
            Property(t => t.ForDate).HasColumnName("ForDate");
            Property(t => t.StaffFullName).HasColumnName("StaffFullName");
            Property(t => t.StaffWorkHoursPerDay).HasColumnName("StaffWorkHoursPerDay");
            Property(t => t.Hours).HasColumnName("Hours");
            Property(t => t.StaffHoursLost).HasColumnName("StaffHoursLost");
            Property(t => t.StaffCost).HasColumnName("StaffCost");
            Property(t => t.StaffBilled).HasColumnName("StaffBilled");
            Property(t => t.StaffProfit).HasColumnName("StaffProfit");
            Property(t => t.StaffProfitLost).HasColumnName("StaffProfitLost");
            Property(t => t.IsInvoiced).HasColumnName("IsInvoiced");
            Property(t => t.FkAccountsId).HasColumnName("fkAccountsID");
            Property(t => t.fkInvoicesID).HasColumnName("fkInvoicesID");
        }
    }
}
