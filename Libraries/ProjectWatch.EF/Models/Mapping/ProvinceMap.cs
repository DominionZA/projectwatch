using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class ProvinceMap : EntityTypeConfiguration<Province>
    {
        public ProvinceMap()
        {
            // Primary Key
            HasKey(t => t.ProvincesId);

            // Properties
            Property(t => t.ProvinceName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Provinces");
            Property(t => t.ProvincesId).HasColumnName("ProvincesID");
            Property(t => t.ProvinceName).HasColumnName("ProvinceName");
        }
    }
}
