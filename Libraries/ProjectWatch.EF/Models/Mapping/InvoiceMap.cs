using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class InvoiceMap : EntityTypeConfiguration<Invoice>
    {
        public InvoiceMap()
        {
            // Primary Key
            HasKey(t => t.InvoicesId);

            // Properties
            Property(t => t.InvoiceNo)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.Terms)
                .HasMaxLength(1000);

            Property(t => t.Notes)
                .HasMaxLength(1000);

            Property(t => t.ClientDetails)
                .HasMaxLength(1000);

            Property(t => t.OrderNo)
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Invoices");
            Property(t => t.InvoicesId).HasColumnName("InvoicesID");
            Property(t => t.Guid).HasColumnName("Guid");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.DateInserted).HasColumnName("DateInserted");
            Property(t => t.InvoiceDate).HasColumnName("InvoiceDate");
            Property(t => t.DueDate).HasColumnName("DueDate");
            Property(t => t.VATPercent).HasColumnName("VATPercent");
            Property(t => t.InvoiceNo).HasColumnName("InvoiceNo");
            Property(t => t.Terms).HasColumnName("Terms");
            Property(t => t.Notes).HasColumnName("Notes");
            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.ClientDetails).HasColumnName("ClientDetails");
            Property(t => t.OrderNo).HasColumnName("OrderNo");

            // Relationships
            HasRequired(t => t.Account)
                .WithMany(t => t.Invoices)
                .HasForeignKey(d => d.fkAccountsID);
            HasRequired(t => t.User)
                .WithMany(t => t.Invoices)
                .HasForeignKey(d => d.fkUsersID);

        }
    }
}
