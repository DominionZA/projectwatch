using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class C__ProjectActivitiesInProjectsMap : EntityTypeConfiguration<C__ProjectActivitiesInProjects>
    {
        public C__ProjectActivitiesInProjectsMap()
        {
            // Primary Key
            HasKey(t => t.ProjectActivitiesInProjectsId);

            // Properties
            // Table & Column Mappings
            ToTable("__ProjectActivitiesInProjects");
            Property(t => t.ProjectActivitiesInProjectsId).HasColumnName("ProjectActivitiesInProjectsID");
            Property(t => t.fkProjectsID).HasColumnName("fkProjectsID");
            Property(t => t.fkProjectActivitiesID).HasColumnName("fkProjectActivitiesID");
            Property(t => t.BillRate).HasColumnName("BillRate");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
        }
    }
}
