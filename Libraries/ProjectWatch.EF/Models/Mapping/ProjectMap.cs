using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class ProjectMap : EntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            // Primary Key
            HasKey(t => t.ProjectsId);

            // Properties
            Property(t => t.ProjectName)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.ProjectDescription)
                .HasMaxLength(2000);

            // Table & Column Mappings
            ToTable("Projects");
            Property(t => t.ProjectsId).HasColumnName("ProjectsID");
            Property(t => t.ProjectName).HasColumnName("ProjectName");
            Property(t => t.ProjectDescription).HasColumnName("ProjectDescription");
            Property(t => t.fkUsersID_Client).HasColumnName("fkUsersID_Client");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.StartDate).HasColumnName("StartDate");
            Property(t => t.EndDate).HasColumnName("EndDate");
            Property(t => t.Budget).HasColumnName("Budget");
            Property(t => t.BudgetHours).HasColumnName("BudgetHours");
        }
    }
}
