using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwTicketMap : EntityTypeConfiguration<VwTicket>
    {
        public VwTicketMap()
        {
            // Primary Key
            HasKey(t => new { t.TicketsID, t.fkUsersID, t.fkAccountsID, t.Subject, t.StatusDescription });

            // Properties
            Property(t => t.FromUser)
                .HasMaxLength(101);

            Property(t => t.FromAccountName)
                .HasMaxLength(100);

            Property(t => t.FromEmail)
                .HasMaxLength(200);

            Property(t => t.ToAccount)
                .HasMaxLength(100);

            Property(t => t.TicketsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkUsersID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkAccountsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(250);

            Property(t => t.TicketNo)
                .HasMaxLength(20);

            Property(t => t.StatusDescription)
                .IsRequired()
                .HasMaxLength(9);

            // Table & Column Mappings
            ToTable("vwTickets");
            Property(t => t.FromUser).HasColumnName("FromUser");
            Property(t => t.FromAccountName).HasColumnName("FromAccountName");
            Property(t => t.FromEmail).HasColumnName("FromEmail");
            Property(t => t.FromAccountID).HasColumnName("FromAccountID");
            Property(t => t.ToAccount).HasColumnName("ToAccount");
            Property(t => t.TicketsID).HasColumnName("TicketsID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.DatePosted).HasColumnName("DatePosted");
            Property(t => t.Subject).HasColumnName("Subject");
            Property(t => t.StatusCode).HasColumnName("StatusCode");
            Property(t => t.TicketNo).HasColumnName("TicketNo");
            Property(t => t.fkUsersID_Operator).HasColumnName("fkUsersID_Operator");
            Property(t => t.StatusDescription).HasColumnName("StatusDescription");
        }
    }
}
