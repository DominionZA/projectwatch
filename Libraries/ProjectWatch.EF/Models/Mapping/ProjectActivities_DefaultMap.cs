using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class ProjectActivities_DefaultMap : EntityTypeConfiguration<ProjectActivities_Default>
    {
        public ProjectActivities_DefaultMap()
        {
            // Primary Key
            HasKey(t => t.ProjectActivitiesDefaultId);

            // Properties
            Property(t => t.ProjectActivityName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("ProjectActivities_Default");
            Property(t => t.ProjectActivitiesDefaultId).HasColumnName("ProjectActivities_DefaultID");
            Property(t => t.ProjectActivityName).HasColumnName("ProjectActivityName");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
            Property(t => t.BillRate).HasColumnName("BillRate");
        }
    }
}
