using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwTimesheetBillingAndCostingRuleMap : EntityTypeConfiguration<VwTimesheetBillingAndCostingRule>
    {
        public VwTimesheetBillingAndCostingRuleMap()
        {
            // Primary Key
            HasKey(t => new { t.TimesheetBillingAndCostingRulesID, t.fkAccountsID, t.RuleType, t.ApplyRule });

            // Properties
            Property(t => t.TimesheetBillingAndCostingRulesID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.fkAccountsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.RuleType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.ApplyRule)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.RuleDescription)
                .HasMaxLength(12);

            Property(t => t.ClientCompanyName)
                .HasMaxLength(100);

            Property(t => t.ProjectName)
                .HasMaxLength(100);

            Property(t => t.ProjectActivityName)
                .HasMaxLength(100);

            Property(t => t.StaffFullName)
                .HasMaxLength(101);

            // Table & Column Mappings
            ToTable("vwTimesheetBillingAndCostingRules");
            Property(t => t.TimesheetBillingAndCostingRulesID).HasColumnName("TimesheetBillingAndCostingRulesID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.RuleType).HasColumnName("RuleType");
            Property(t => t.Value).HasColumnName("Value");
            Property(t => t.ApplyRule).HasColumnName("ApplyRule");
            Property(t => t.RuleDescription).HasColumnName("RuleDescription");
            Property(t => t.fkProjectActivitiesID).HasColumnName("fkProjectActivitiesID");
            Property(t => t.fkProjectsID).HasColumnName("fkProjectsID");
            Property(t => t.fkUsersID_Client).HasColumnName("fkUsersID_Client");
            Property(t => t.fkUsersID_Staff).HasColumnName("fkUsersID_Staff");
            Property(t => t.ClientCompanyName).HasColumnName("ClientCompanyName");
            Property(t => t.ProjectName).HasColumnName("ProjectName");
            Property(t => t.ProjectActivityName).HasColumnName("ProjectActivityName");
            Property(t => t.StaffFullName).HasColumnName("StaffFullName");
        }
    }
}
