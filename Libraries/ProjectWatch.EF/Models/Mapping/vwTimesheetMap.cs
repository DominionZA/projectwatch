using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwTimesheetMap : EntityTypeConfiguration<VwTimesheet>
    {
        public VwTimesheetMap()
        {
            // Primary Key
            HasKey(t => new { TimesheetsID = t.TimesheetsId, t.fkUsersID_Staff, t.ForDate, t.Hours, t.Description, t.BillRate, t.CostRate });

            // Properties
            Property(t => t.TimesheetsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkUsersID_Staff)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Description)
                .IsRequired();

            Property(t => t.ProjectName)
                .HasMaxLength(100);

            Property(t => t.StaffFullName)
                .HasMaxLength(101);

            Property(t => t.ProjectActivityName)
                .HasMaxLength(100);

            Property(t => t.BillRate)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.CostRate)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.ClientCompanyName)
                .HasMaxLength(100);

            Property(t => t.ShortClientCompanyName)
                .HasMaxLength(100);

            Property(t => t.InvoiceNo)
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("vwTimesheets");
            Property(t => t.TimesheetsId).HasColumnName("TimesheetsID");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
            Property(t => t.fkUsersID_Staff).HasColumnName("fkUsersID_Staff");
            Property(t => t.fkProjectActivitiesID).HasColumnName("fkProjectActivitiesID");
            Property(t => t.ForDate).HasColumnName("ForDate");
            Property(t => t.Hours).HasColumnName("Hours");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.IsApproved).HasColumnName("IsApproved");
            Property(t => t.ShortDescription).HasColumnName("ShortDescription");
            Property(t => t.fkInvoicesID).HasColumnName("fkInvoicesID");
            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.ProjectsId).HasColumnName("ProjectsID");
            Property(t => t.AccountsId).HasColumnName("AccountsID");
            Property(t => t.fkUsersID_Client).HasColumnName("fkUsersID_Client");
            Property(t => t.ProjectName).HasColumnName("ProjectName");
            Property(t => t.ProjectStartDate).HasColumnName("ProjectStartDate");
            Property(t => t.ProjectEndDate).HasColumnName("ProjectEndDate");
            Property(t => t.Budget).HasColumnName("Budget");
            Property(t => t.BudgetHours).HasColumnName("BudgetHours");
            Property(t => t.StaffFullName).HasColumnName("StaffFullName");
            Property(t => t.ProjectActivityName).HasColumnName("ProjectActivityName");
            Property(t => t.BillRate).HasColumnName("BillRate");
            Property(t => t.CostRate).HasColumnName("CostRate");
            Property(t => t.TotalBilled).HasColumnName("TotalBilled");
            Property(t => t.TotalCost).HasColumnName("TotalCost");
            Property(t => t.TotalProfit).HasColumnName("TotalProfit");
            Property(t => t.ClientCompanyName).HasColumnName("ClientCompanyName");
            Property(t => t.ShortClientCompanyName).HasColumnName("ShortClientCompanyName");
            Property(t => t.InvoiceNo).HasColumnName("InvoiceNo");
        }
    }
}
