using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class ProductCategoryMap : EntityTypeConfiguration<ProductCategory>
    {
        public ProductCategoryMap()
        {
            // Primary Key
            HasKey(t => t.ProductCategoriesId);

            // Properties
            Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("ProductCategories");
            Property(t => t.ProductCategoriesId).HasColumnName("ProductCategoriesID");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
        }
    }
}
