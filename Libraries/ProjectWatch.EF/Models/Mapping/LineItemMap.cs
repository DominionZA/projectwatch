using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class LineItemMap : EntityTypeConfiguration<LineItem>
    {
        public LineItemMap()
        {
            // Primary Key
            HasKey(t => t.LineItemsId);

            // Properties
            Property(t => t.ItemCode)
                .HasMaxLength(10);

            Property(t => t.ItemDescription)
                .IsRequired()
                .HasMaxLength(1000);

            Property(t => t.Measure)
                .HasMaxLength(40);

            // Table & Column Mappings
            ToTable("LineItems");
            Property(t => t.LineItemsId).HasColumnName("LineItemsID");
            Property(t => t.fkInvoicesID).HasColumnName("fkInvoicesID");
            Property(t => t.fkItemsID).HasColumnName("fkItemsID");
            Property(t => t.ItemCode).HasColumnName("ItemCode");
            Property(t => t.ItemDescription).HasColumnName("ItemDescription");
            Property(t => t.ItemPriceExcl).HasColumnName("ItemPriceExcl");
            Property(t => t.ItemCostExcl).HasColumnName("ItemCostExcl");
            Property(t => t.ItemDiscount).HasColumnName("ItemDiscount");
            Property(t => t.ItemQuantity).HasColumnName("ItemQuantity");
            Property(t => t.Measure).HasColumnName("Measure");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
            Property(t => t.ItemTotalExcl).HasColumnName("ItemTotalExcl");

            // Relationships
            HasRequired(t => t.Invoice)
                .WithMany(t => t.LineItems)
                .HasForeignKey(d => d.fkInvoicesID);

        }
    }
}
