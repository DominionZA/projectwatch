using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwBillingAndCostingRuleMap : EntityTypeConfiguration<VwBillingAndCostingRule>
    {
        public VwBillingAndCostingRuleMap()
        {
            // Primary Key
            HasKey(t => new { TimesheetBillingAndCostingRulesID = t.TimesheetBillingAndCostingRulesId, t.fkAccountsID, t.RuleType, t.ApplyRule });

            // Properties
            Property(t => t.TimesheetBillingAndCostingRulesId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.fkAccountsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.RuleType)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.ApplyRule)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.RuleDescription)
                .HasMaxLength(32);

            // Table & Column Mappings
            ToTable("vwBillingAndCostingRules");
            Property(t => t.TimesheetBillingAndCostingRulesId).HasColumnName("TimesheetBillingAndCostingRulesID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.fkUsersID_Client).HasColumnName("fkUsersID_Client");
            Property(t => t.fkProjectsID).HasColumnName("fkProjectsID");
            Property(t => t.fkProjectActivitiesID).HasColumnName("fkProjectActivitiesID");
            Property(t => t.fkUsersID_Staff).HasColumnName("fkUsersID_Staff");
            Property(t => t.RuleType).HasColumnName("RuleType");
            Property(t => t.Value).HasColumnName("Value");
            Property(t => t.ApplyRule).HasColumnName("ApplyRule");
            Property(t => t.RuleDescription).HasColumnName("RuleDescription");
        }
    }
}
