using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwTicketHistoryMap : EntityTypeConfiguration<VwTicketHistory>
    {
        public VwTicketHistoryMap()
        {
            // Primary Key
            HasKey(t => new { t.TicketHistoryID, t.fkTicketsID, t.fkUsersID });

            // Properties
            Property(t => t.FromUser)
                .HasMaxLength(101);

            Property(t => t.FromAccountName)
                .HasMaxLength(100);

            Property(t => t.FromEmail)
                .HasMaxLength(200);

            Property(t => t.ToAccount)
                .HasMaxLength(100);

            Property(t => t.TicketHistoryID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkTicketsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkUsersID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Body)
                .HasMaxLength(6000);

            // Table & Column Mappings
            ToTable("vwTicketHistory");
            Property(t => t.FromUser).HasColumnName("FromUser");
            Property(t => t.FromAccountName).HasColumnName("FromAccountName");
            Property(t => t.FromEmail).HasColumnName("FromEmail");
            Property(t => t.FromAccountID).HasColumnName("FromAccountID");
            Property(t => t.ToAccount).HasColumnName("ToAccount");
            Property(t => t.TicketHistoryID).HasColumnName("TicketHistoryID");
            Property(t => t.fkTicketsID).HasColumnName("fkTicketsID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.DatePosted).HasColumnName("DatePosted");
            Property(t => t.Body).HasColumnName("Body");
        }
    }
}
