using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class GatewayMap : EntityTypeConfiguration<Gateway>
    {
        public GatewayMap()
        {
            // Primary Key
            HasKey(t => t.GatewaysId);

            // Properties
            Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(250);

            Property(t => t.URL)
                .HasMaxLength(256);

            Property(t => t.Summary)
                .HasMaxLength(500);

            // Table & Column Mappings
            ToTable("Gateways");
            Property(t => t.GatewaysId).HasColumnName("GatewaysID");
            Property(t => t.Code).HasColumnName("Code");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.URL).HasColumnName("URL");
            Property(t => t.Summary).HasColumnName("Summary");
        }
    }
}
