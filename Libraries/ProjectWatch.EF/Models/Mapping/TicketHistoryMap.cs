using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class TicketHistoryMap : EntityTypeConfiguration<TicketHistory>
    {
        public TicketHistoryMap()
        {
            // Primary Key
            HasKey(t => t.TicketHistoryId);

            // Properties
            Property(t => t.Body)
                .HasMaxLength(6000);

            // Table & Column Mappings
            ToTable("TicketHistory");
            Property(t => t.TicketHistoryId).HasColumnName("TicketHistoryID");
            Property(t => t.fkTicketsID).HasColumnName("fkTicketsID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.DatePosted).HasColumnName("DatePosted");
            Property(t => t.Body).HasColumnName("Body");
        }
    }
}
