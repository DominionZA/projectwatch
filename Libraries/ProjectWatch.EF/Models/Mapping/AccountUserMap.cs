using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class AccountUserMap : EntityTypeConfiguration<AccountUser>
    {
        public AccountUserMap()
        {
            // Primary Key
            HasKey(t => t.AccountUsersID);

            // Properties
            // Table & Column Mappings
            ToTable("AccountUsers");
            Property(t => t.AccountUsersID).HasColumnName("AccountUsersID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
        }
    }
}
