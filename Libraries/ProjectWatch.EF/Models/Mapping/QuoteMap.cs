using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class QuoteMap : EntityTypeConfiguration<Quote>
    {
        public QuoteMap()
        {
            // Primary Key
            HasKey(t => new { QuotesID = t.QuotesId, t.fkUsersID, t.fkAccountsID, t.QuoteDate, t.VATPercent, t.InvoiceNo });

            // Properties
            Property(t => t.QuotesId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.fkUsersID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkAccountsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.InvoiceNo)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.Notes)
                .HasMaxLength(1000);

            Property(t => t.ClientDetails)
                .HasMaxLength(1000);

            // Table & Column Mappings
            ToTable("Quotes");
            Property(t => t.QuotesId).HasColumnName("QuotesID");
            Property(t => t.Guid).HasColumnName("Guid");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.DateInserted).HasColumnName("DateInserted");
            Property(t => t.QuoteDate).HasColumnName("QuoteDate");
            Property(t => t.QuoteValidUntil).HasColumnName("QuoteValidUntil");
            Property(t => t.VATPercent).HasColumnName("VATPercent");
            Property(t => t.InvoiceNo).HasColumnName("InvoiceNo");
            Property(t => t.Notes).HasColumnName("Notes");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.ClientDetails).HasColumnName("ClientDetails");
        }
    }
}
