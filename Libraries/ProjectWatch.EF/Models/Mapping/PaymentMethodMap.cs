using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class PaymentMethodMap : EntityTypeConfiguration<PaymentMethod>
    {
        public PaymentMethodMap()
        {
            // Primary Key
            HasKey(t => t.PaymentMethodsId);

            // Properties
            Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("PaymentMethods");
            Property(t => t.PaymentMethodsId).HasColumnName("PaymentMethodsID");
            Property(t => t.Description).HasColumnName("Description");
        }
    }
}
