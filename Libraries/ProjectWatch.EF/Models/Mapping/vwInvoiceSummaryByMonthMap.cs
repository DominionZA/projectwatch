using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwInvoiceSummaryByMonthMap : EntityTypeConfiguration<VwInvoiceSummaryByMonth>
    {
        public VwInvoiceSummaryByMonthMap()
        {
            // Primary Key
            HasKey(t => t.fkAccountsID);

            // Properties
            Property(t => t.fkAccountsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("vwInvoiceSummaryByMonth");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.Year).HasColumnName("Year");
            Property(t => t.Month).HasColumnName("Month");
            Property(t => t.SumInvoiceSubTotalExcl).HasColumnName("SumInvoiceSubTotalExcl");
            Property(t => t.SumInvoiceVatAmount).HasColumnName("SumInvoiceVATAmount");
            Property(t => t.SumInvoiceTotalIncl).HasColumnName("SumInvoiceTotalIncl");
            Property(t => t.SumInvoicePaidToDate).HasColumnName("SumInvoicePaidToDate");
            Property(t => t.SumInvoiceBalanceIncl).HasColumnName("SumInvoiceBalanceIncl");
        }
    }
}
