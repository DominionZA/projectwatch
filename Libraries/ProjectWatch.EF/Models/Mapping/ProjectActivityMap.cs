using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class ProjectActivityMap : EntityTypeConfiguration<ProjectActivity>
    {
        public ProjectActivityMap()
        {
            // Primary Key
            HasKey(t => t.ProjectActivitiesId);

            // Properties
            Property(t => t.ProjectActivityName)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            ToTable("ProjectActivities");
            Property(t => t.ProjectActivitiesId).HasColumnName("ProjectActivitiesID");
            Property(t => t.ProjectActivityName).HasColumnName("ProjectActivityName");
            Property(t => t.fkProjectsID).HasColumnName("fkProjectsID");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
            Property(t => t.BillRate).HasColumnName("BillRate");

            // Relationships
            HasRequired(t => t.Project)
                .WithMany(t => t.ProjectActivities)
                .HasForeignKey(d => d.fkProjectsID);

        }
    }
}
