using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class AccountLinkGatewayMap : EntityTypeConfiguration<AccountLinkGateway>
    {
        public AccountLinkGatewayMap()
        {
            // Primary Key
            HasKey(t => t.AccountLinkGatewayID);

            // Properties
            Property(t => t.MerchantNo)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("AccountLinkGateway");
            Property(t => t.AccountLinkGatewayID).HasColumnName("AccountLinkGatewayID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.fkGatewaysID).HasColumnName("fkGatewaysID");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.MerchantNo).HasColumnName("MerchantNo");

            // Relationships
            HasRequired(t => t.Account)
                .WithMany(t => t.AccountLinkGateways)
                .HasForeignKey(d => d.fkAccountsID);
            HasRequired(t => t.Gateway)
                .WithMany(t => t.AccountLinkGateways)
                .HasForeignKey(d => d.fkGatewaysID);

        }
    }
}
