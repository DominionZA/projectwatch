using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class TicketMap : EntityTypeConfiguration<Ticket>
    {
        public TicketMap()
        {
            // Primary Key
            HasKey(t => t.TicketsId);

            // Properties
            Property(t => t.Subject)
                .IsRequired()
                .HasMaxLength(250);

            Property(t => t.TicketNo)
                .HasMaxLength(20);

            // Table & Column Mappings
            ToTable("Tickets");
            Property(t => t.TicketsId).HasColumnName("TicketsID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.DatePosted).HasColumnName("DatePosted");
            Property(t => t.Subject).HasColumnName("Subject");
            Property(t => t.TicketNo).HasColumnName("TicketNo");
            Property(t => t.StatusCode).HasColumnName("StatusCode");
            Property(t => t.fkUsersID_Operator).HasColumnName("fkUsersID_Operator");
            Property(t => t.GUID).HasColumnName("GUID");
        }
    }
}
