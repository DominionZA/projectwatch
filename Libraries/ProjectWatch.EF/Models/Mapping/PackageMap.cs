using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class PackageMap : EntityTypeConfiguration<Package>
    {
        public PackageMap()
        {
            // Primary Key
            HasKey(t => t.PackagesId);

            // Properties
            Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            ToTable("Packages");
            Property(t => t.PackagesId).HasColumnName("PackagesID");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.Price).HasColumnName("Price");
            Property(t => t.MaxCustomers).HasColumnName("MaxCustomers");
        }
    }
}
