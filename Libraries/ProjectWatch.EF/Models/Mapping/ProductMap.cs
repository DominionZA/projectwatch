using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class ProductMap : EntityTypeConfiguration<Product>
    {
        public ProductMap()
        {
            // Primary Key
            HasKey(t => t.ProductsId);

            // Properties
            Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(10);

            Property(t => t.Measure)
                .HasMaxLength(40);

            // Table & Column Mappings
            ToTable("Products");
            Property(t => t.ProductsId).HasColumnName("ProductsID");
            Property(t => t.fkProductCategoriesID).HasColumnName("fkProductCategoriesID");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.SellingPrice).HasColumnName("SellingPrice");
            Property(t => t.CostPrice).HasColumnName("CostPrice");
            Property(t => t.Code).HasColumnName("Code");
            Property(t => t.OnHand).HasColumnName("OnHand");
            Property(t => t.Measure).HasColumnName("Measure");
        }
    }
}
