using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class AddInUserMap : EntityTypeConfiguration<AddInUser>
    {
        public AddInUserMap()
        {
            // Primary Key
            HasKey(t => t.AddInUsersID);

            // Properties
            Property(t => t.AddInVersion)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            ToTable("AddInUsers");
            Property(t => t.AddInUsersID).HasColumnName("AddInUsersID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.AddInVersion).HasColumnName("AddInVersion");
        }
    }
}
