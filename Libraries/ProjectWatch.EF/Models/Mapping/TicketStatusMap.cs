using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class TicketStatusMap : EntityTypeConfiguration<TicketStatus>
    {
        public TicketStatusMap()
        {
            // Primary Key
            HasKey(t => t.StatusCode);

            // Properties
            Property(t => t.TicketStatusId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.StatusCode)
                .IsRequired()
                .HasMaxLength(10);

            Property(t => t.StatusDescription)
                .IsRequired()
                .HasMaxLength(250);

            // Table & Column Mappings
            ToTable("TicketStatus");
            Property(t => t.TicketStatusId).HasColumnName("TicketStatusID");
            Property(t => t.StatusCode).HasColumnName("StatusCode");
            Property(t => t.StatusDescription).HasColumnName("StatusDescription");
        }
    }
}
