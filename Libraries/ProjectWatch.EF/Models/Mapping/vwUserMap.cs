using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwUserMap : EntityTypeConfiguration<VwUser>
    {
        public VwUserMap()
        {
            // Primary Key
            HasKey(t => new { UsersID = t.UsersId, UserGUID = t.UserGuid, t.fkAccountsID, t.Email, t.Active });

            // Properties
            Property(t => t.DisplayName)
                .HasMaxLength(204);

            Property(t => t.DisplayCompany)
                .HasMaxLength(204);

            Property(t => t.UsersId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.UserCode)
                .HasMaxLength(50);

            Property(t => t.CompanyName)
                .HasMaxLength(100);

            Property(t => t.UserName)
                .HasMaxLength(50);

            Property(t => t.Password)
                .HasMaxLength(50);

            Property(t => t.FirstName)
                .HasMaxLength(50);

            Property(t => t.LastName)
                .HasMaxLength(50);

            Property(t => t.DayPhone)
                .HasMaxLength(20);

            Property(t => t.HomePhone)
                .HasMaxLength(20);

            Property(t => t.CellPhone)
                .HasMaxLength(20);

            Property(t => t.Fax)
                .HasMaxLength(20);

            Property(t => t.PhysAddress1)
                .HasMaxLength(100);

            Property(t => t.PhysAddress2)
                .HasMaxLength(100);

            Property(t => t.PhysCity)
                .HasMaxLength(50);

            Property(t => t.PhysProvince)
                .HasMaxLength(50);

            Property(t => t.PhysPostalCode)
                .HasMaxLength(10);

            Property(t => t.PhysCountry)
                .HasMaxLength(50);

            Property(t => t.PostAddress1)
                .HasMaxLength(100);

            Property(t => t.PostAddress2)
                .HasMaxLength(100);

            Property(t => t.PostCity)
                .HasMaxLength(50);

            Property(t => t.PostProvince)
                .HasMaxLength(50);

            Property(t => t.PostPostalCode)
                .HasMaxLength(10);

            Property(t => t.PostCountry)
                .HasMaxLength(50);

            Property(t => t.fkAccountsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(200);

            Property(t => t.Notes)
                .HasMaxLength(2000);

            Property(t => t.FullName)
                .HasMaxLength(101);

            Property(t => t.FormalName)
                .HasMaxLength(101);

            Property(t => t.VatNo)
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("vwUsers");
            Property(t => t.DisplayName).HasColumnName("DisplayName");
            Property(t => t.DisplayCompany).HasColumnName("DisplayCompany");
            Property(t => t.UsersId).HasColumnName("UsersID");
            Property(t => t.UserGuid).HasColumnName("UserGUID");
            Property(t => t.UserCode).HasColumnName("UserCode");
            Property(t => t.CompanyName).HasColumnName("CompanyName");
            Property(t => t.UserName).HasColumnName("UserName");
            Property(t => t.Password).HasColumnName("Password");
            Property(t => t.FirstName).HasColumnName("FirstName");
            Property(t => t.LastName).HasColumnName("LastName");
            Property(t => t.DayPhone).HasColumnName("DayPhone");
            Property(t => t.HomePhone).HasColumnName("HomePhone");
            Property(t => t.CellPhone).HasColumnName("CellPhone");
            Property(t => t.Fax).HasColumnName("Fax");
            Property(t => t.PhysAddress1).HasColumnName("PhysAddress1");
            Property(t => t.PhysAddress2).HasColumnName("PhysAddress2");
            Property(t => t.PhysCity).HasColumnName("PhysCity");
            Property(t => t.PhysProvince).HasColumnName("PhysProvince");
            Property(t => t.PhysPostalCode).HasColumnName("PhysPostalCode");
            Property(t => t.PhysCountry).HasColumnName("PhysCountry");
            Property(t => t.PostAddress1).HasColumnName("PostAddress1");
            Property(t => t.PostAddress2).HasColumnName("PostAddress2");
            Property(t => t.PostCity).HasColumnName("PostCity");
            Property(t => t.PostProvince).HasColumnName("PostProvince");
            Property(t => t.PostPostalCode).HasColumnName("PostPostalCode");
            Property(t => t.PostCountry).HasColumnName("PostCountry");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.Validated).HasColumnName("Validated");
            Property(t => t.AccessLevel).HasColumnName("AccessLevel");
            Property(t => t.DateCreated).HasColumnName("DateCreated");
            Property(t => t.LastLoginDate).HasColumnName("LastLoginDate");
            Property(t => t.Notes).HasColumnName("Notes");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.FullName).HasColumnName("FullName");
            Property(t => t.FormalName).HasColumnName("FormalName");
            Property(t => t.VatNo).HasColumnName("VatNo");
            Property(t => t.BillRate).HasColumnName("BillRate");
            Property(t => t.CostRate).HasColumnName("CostRate");
        }
    }
}
