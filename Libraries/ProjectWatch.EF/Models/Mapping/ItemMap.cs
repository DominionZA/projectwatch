using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class ItemMap : EntityTypeConfiguration<Item>
    {
        public ItemMap()
        {
            // Primary Key
            HasKey(t => t.ItemsId);

            // Properties
            Property(t => t.ItemDescription)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            ToTable("Items");
            Property(t => t.ItemsId).HasColumnName("ItemsID");
            Property(t => t.ItemDescription).HasColumnName("ItemDescription");
            Property(t => t.ItemCostIncl).HasColumnName("ItemCostIncl");
            Property(t => t.ItemPriceIncl).HasColumnName("ItemPriceIncl");
        }
    }
}
