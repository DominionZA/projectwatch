using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwProductMap : EntityTypeConfiguration<VwProduct>
    {
        public VwProductMap()
        {
            // Primary Key
            HasKey(t => new { ProductsID = t.ProductsId, t.fkProductCategoriesID, t.Description, t.SellingPrice, t.CostPrice, t.Code });

            // Properties
            Property(t => t.DisplayCpc)
                .HasMaxLength(216);

            Property(t => t.DisplayCp)
                .HasMaxLength(203);

            Property(t => t.Category)
                .HasMaxLength(100);

            Property(t => t.ProductsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkProductCategoriesID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Description)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.SellingPrice)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.CostPrice)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.Code)
                .IsRequired()
                .HasMaxLength(10);

            Property(t => t.Measure)
                .HasMaxLength(40);

            // Table & Column Mappings
            ToTable("vwProducts");
            Property(t => t.DisplayCpc).HasColumnName("DisplayCPC");
            Property(t => t.DisplayCp).HasColumnName("DisplayCP");
            Property(t => t.Category).HasColumnName("Category");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.ProductsId).HasColumnName("ProductsID");
            Property(t => t.fkProductCategoriesID).HasColumnName("fkProductCategoriesID");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.SellingPrice).HasColumnName("SellingPrice");
            Property(t => t.CostPrice).HasColumnName("CostPrice");
            Property(t => t.Code).HasColumnName("Code");
            Property(t => t.OnHand).HasColumnName("OnHand");
            Property(t => t.Measure).HasColumnName("Measure");
        }
    }
}
