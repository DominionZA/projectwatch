using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class SolutionTimeMap : EntityTypeConfiguration<SolutionTime>
    {
        public SolutionTimeMap()
        {
            // Primary Key
            HasKey(t => t.SolutionTimesId);

            // Properties
            Property(t => t.SolutionFullName)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            ToTable("SolutionTimes");
            Property(t => t.SolutionTimesId).HasColumnName("SolutionTimesID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.SolutionFullName).HasColumnName("SolutionFullName");
            Property(t => t.SessionHours).HasColumnName("SessionHours");
            Property(t => t.TotalHours).HasColumnName("TotalHours");
        }
    }
}
