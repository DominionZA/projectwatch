using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwClientStatementMap : EntityTypeConfiguration<VwClientStatement>
    {
        public VwClientStatementMap()
        {
            // Primary Key
            HasKey(t => new { InvoicesID = t.InvoicesId, t.fkUsersID, t.InvoiceNo, t.ActionDate, t.IsReversal });

            // Properties
            Property(t => t.InvoicesId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkUsersID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.InvoiceNo)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.IsReversal)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("vwClientStatement");
            Property(t => t.InvoicesId).HasColumnName("InvoicesID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.InvoiceNo).HasColumnName("InvoiceNo");
            Property(t => t.ActionDate).HasColumnName("ActionDate");
            Property(t => t.Amount).HasColumnName("Amount");
            Property(t => t.DebitAmount).HasColumnName("DebitAmount");
            Property(t => t.CreditAmount).HasColumnName("CreditAmount");
            Property(t => t.IsReversal).HasColumnName("IsReversal");
        }
    }
}
