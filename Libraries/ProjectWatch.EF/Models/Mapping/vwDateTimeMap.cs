using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwDateTimeMap : EntityTypeConfiguration<VwDateTime>
    {
        public VwDateTimeMap()
        {
            // Primary Key
            HasKey(t => t.CurrentDateTime);

            // Properties
            // Table & Column Mappings
            ToTable("vwDateTime");
            Property(t => t.CurrentDateTime).HasColumnName("CurrentDateTime");
        }
    }
}
