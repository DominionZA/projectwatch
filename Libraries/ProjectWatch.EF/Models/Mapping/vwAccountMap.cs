using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwAccountMap : EntityTypeConfiguration<VwAccount>
    {
        public VwAccountMap()
        {
            // Primary Key
            HasKey(t => new { AccountsID = t.AccountsId, t.AccountName, t.AccountSerial, t.Active, t.InvoiceNoSeed, t.InvoiceNoLength, InvoiceVATPercent = t.InvoiceVatPercent, t.Package });

            // Properties
            Property(t => t.AccountsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.AccountName)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.AccountSerial)
                .IsRequired()
                .HasMaxLength(50);

            Property(t => t.AccountLogoPath)
                .HasMaxLength(255);

            Property(t => t.InvoiceNoPrefix)
                .HasMaxLength(20);

            Property(t => t.InvoiceNoSuffix)
                .HasMaxLength(20);

            Property(t => t.InvoiceNoSeed)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.InvoiceNoLength)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.PhysAddress1)
                .HasMaxLength(100);

            Property(t => t.PhysAddress2)
                .HasMaxLength(100);

            Property(t => t.PhysCity)
                .HasMaxLength(50);

            Property(t => t.PhysProvince)
                .HasMaxLength(50);

            Property(t => t.PhysCountry)
                .HasMaxLength(50);

            Property(t => t.PhysPostalCode)
                .HasMaxLength(5);

            Property(t => t.DayPhone)
                .HasMaxLength(20);

            Property(t => t.MobilePhone)
                .HasMaxLength(20);

            Property(t => t.FaxNo)
                .HasMaxLength(20);

            Property(t => t.Email)
                .HasMaxLength(200);

            Property(t => t.EmailSalutation)
                .HasMaxLength(2000);

            Property(t => t.DefaultTerms)
                .HasMaxLength(1000);

            Property(t => t.DefaultNotes)
                .HasMaxLength(1000);

            Property(t => t.VatNo)
                .HasMaxLength(50);

            Property(t => t.CkNo)
                .HasMaxLength(50);

            Property(t => t.Package)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            ToTable("vwAccounts");
            Property(t => t.AccountsId).HasColumnName("AccountsID");
            Property(t => t.AccountName).HasColumnName("AccountName");
            Property(t => t.AccountSerial).HasColumnName("AccountSerial");
            Property(t => t.DateCreated).HasColumnName("DateCreated");
            Property(t => t.AccountLogoPath).HasColumnName("AccountLogoPath");
            Property(t => t.Active).HasColumnName("Active");
            Property(t => t.InvoiceNoPrefix).HasColumnName("InvoiceNoPrefix");
            Property(t => t.InvoiceNoSuffix).HasColumnName("InvoiceNoSuffix");
            Property(t => t.InvoiceNoSeed).HasColumnName("InvoiceNoSeed");
            Property(t => t.InvoiceNoLength).HasColumnName("InvoiceNoLength");
            Property(t => t.InvoiceVatPercent).HasColumnName("InvoiceVATPercent");
            Property(t => t.PhysAddress1).HasColumnName("PhysAddress1");
            Property(t => t.PhysAddress2).HasColumnName("PhysAddress2");
            Property(t => t.PhysCity).HasColumnName("PhysCity");
            Property(t => t.PhysProvince).HasColumnName("PhysProvince");
            Property(t => t.PhysCountry).HasColumnName("PhysCountry");
            Property(t => t.PhysPostalCode).HasColumnName("PhysPostalCode");
            Property(t => t.DayPhone).HasColumnName("DayPhone");
            Property(t => t.MobilePhone).HasColumnName("MobilePhone");
            Property(t => t.FaxNo).HasColumnName("FaxNo");
            Property(t => t.Email).HasColumnName("Email");
            Property(t => t.EmailSalutation).HasColumnName("EmailSalutation");
            Property(t => t.DefaultTerms).HasColumnName("DefaultTerms");
            Property(t => t.DefaultNotes).HasColumnName("DefaultNotes");
            Property(t => t.VatNo).HasColumnName("VATNo");
            Property(t => t.CkNo).HasColumnName("CKNo");
            Property(t => t.HomePage).HasColumnName("HomePage");
            Property(t => t.fkPackagesID).HasColumnName("fkPackagesID");
            Property(t => t.Package).HasColumnName("Package");
        }
    }
}
