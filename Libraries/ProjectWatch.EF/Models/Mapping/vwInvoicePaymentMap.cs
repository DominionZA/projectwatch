using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class VwInvoicePaymentMap : EntityTypeConfiguration<VwInvoicePayment>
    {
        public VwInvoicePaymentMap()
        {
            // Primary Key
            HasKey(t => new { InvoicePaymentsID = t.InvoicePaymentsId, t.fkInvoicesID, t.PaymentDate, t.fkPaymentMethodsID });

            // Properties
            Property(t => t.InvoicePaymentsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.fkInvoicesID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.PaymentNotes)
                .HasMaxLength(2000);

            Property(t => t.fkPaymentMethodsID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            // Table & Column Mappings
            ToTable("vwInvoicePayments");
            Property(t => t.InvoicePaymentsId).HasColumnName("InvoicePaymentsID");
            Property(t => t.fkInvoicesID).HasColumnName("fkInvoicesID");
            Property(t => t.PaymentDate).HasColumnName("PaymentDate");
            Property(t => t.PaymentNotes).HasColumnName("PaymentNotes");
            Property(t => t.PaymentAmount).HasColumnName("PaymentAmount");
            Property(t => t.fkPaymentMethodsID).HasColumnName("fkPaymentMethodsID");
            Property(t => t.DaysSinceDue).HasColumnName("DaysSinceDue");
        }
    }
}
