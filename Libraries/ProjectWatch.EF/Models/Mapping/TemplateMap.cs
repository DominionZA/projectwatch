using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class TemplateMap : EntityTypeConfiguration<Template>
    {
        public TemplateMap()
        {
            // Primary Key
            HasKey(t => t.TemplatesId);

            // Properties
            Property(t => t.TemplateName)
                .IsRequired()
                .HasMaxLength(50);

            // Table & Column Mappings
            ToTable("Templates");
            Property(t => t.TemplatesId).HasColumnName("TemplatesID");
            Property(t => t.TemplateName).HasColumnName("TemplateName");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.IsPrimary).HasColumnName("IsPrimary");
        }
    }
}
