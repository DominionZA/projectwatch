using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class dtpropertyMap : EntityTypeConfiguration<dtproperty>
    {
        public dtpropertyMap()
        {
            // Primary Key
            HasKey(t => new { id = t.Id, t.property });

            // Properties
            Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.property)
                .IsRequired()
                .HasMaxLength(64);

            Property(t => t.value)
                .HasMaxLength(255);

            Property(t => t.uvalue)
                .HasMaxLength(255);

            // Table & Column Mappings
            ToTable("dtproperties");
            Property(t => t.Id).HasColumnName("id");
            Property(t => t.objectid).HasColumnName("objectid");
            Property(t => t.property).HasColumnName("property");
            Property(t => t.value).HasColumnName("value");
            Property(t => t.uvalue).HasColumnName("uvalue");
            Property(t => t.lvalue).HasColumnName("lvalue");
            Property(t => t.version).HasColumnName("version");
        }
    }
}
