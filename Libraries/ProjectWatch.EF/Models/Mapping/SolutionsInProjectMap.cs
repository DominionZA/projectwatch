using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class SolutionsInProjectMap : EntityTypeConfiguration<SolutionsInProject>
    {
        public SolutionsInProjectMap()
        {
            // Primary Key
            HasKey(t => t.SolutionsInProjectsId);

            // Properties
            Property(t => t.SolutionFullName)
                .IsRequired()
                .HasMaxLength(500);

            // Table & Column Mappings
            ToTable("SolutionsInProjects");
            Property(t => t.SolutionsInProjectsId).HasColumnName("SolutionsInProjectsID");
            Property(t => t.fkUsersID).HasColumnName("fkUsersID");
            Property(t => t.fkProjectsID).HasColumnName("fkProjectsID");
            Property(t => t.SolutionFullName).HasColumnName("SolutionFullName");
        }
    }
}
