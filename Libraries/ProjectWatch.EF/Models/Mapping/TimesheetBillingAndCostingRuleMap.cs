using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class TimesheetBillingAndCostingRuleMap : EntityTypeConfiguration<TimesheetBillingAndCostingRule>
    {
        public TimesheetBillingAndCostingRuleMap()
        {
            // Primary Key
            HasKey(t => t.TimesheetBillingAndCostingRulesId);

            // Properties
            // Table & Column Mappings
            ToTable("TimesheetBillingAndCostingRules");
            Property(t => t.TimesheetBillingAndCostingRulesId).HasColumnName("TimesheetBillingAndCostingRulesID");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
            Property(t => t.fkUsersID_Client).HasColumnName("fkUsersID_Client");
            Property(t => t.fkProjectsID).HasColumnName("fkProjectsID");
            Property(t => t.fkProjectActivitiesID).HasColumnName("fkProjectActivitiesID");
            Property(t => t.fkUsersID_Staff).HasColumnName("fkUsersID_Staff");
            Property(t => t.RuleType).HasColumnName("RuleType");
            Property(t => t.Value).HasColumnName("Value");
            Property(t => t.ApplyRule).HasColumnName("ApplyRule");
        }
    }
}
