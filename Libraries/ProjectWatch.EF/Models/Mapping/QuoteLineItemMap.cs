using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class QuoteLineItemMap : EntityTypeConfiguration<QuoteLineItem>
    {
        public QuoteLineItemMap()
        {
            // Primary Key
            HasKey(t => new { QuoteLineItemsID = t.QuoteLineItemsId, t.fkQuotesID, t.ItemDescription, t.ItemQuantity });

            // Properties
            Property(t => t.QuoteLineItemsId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);

            Property(t => t.fkQuotesID)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            Property(t => t.ItemCode)
                .HasMaxLength(10);

            Property(t => t.ItemDescription)
                .IsRequired()
                .HasMaxLength(1000);

            Property(t => t.Measure)
                .HasMaxLength(40);

            // Table & Column Mappings
            ToTable("QuoteLineItems");
            Property(t => t.QuoteLineItemsId).HasColumnName("QuoteLineItemsID");
            Property(t => t.fkQuotesID).HasColumnName("fkQuotesID");
            Property(t => t.fkItemsID).HasColumnName("fkItemsID");
            Property(t => t.ItemCode).HasColumnName("ItemCode");
            Property(t => t.ItemDescription).HasColumnName("ItemDescription");
            Property(t => t.ItemPriceExcl).HasColumnName("ItemPriceExcl");
            Property(t => t.ItemCostExcl).HasColumnName("ItemCostExcl");
            Property(t => t.ItemDiscount).HasColumnName("ItemDiscount");
            Property(t => t.ItemQuantity).HasColumnName("ItemQuantity");
            Property(t => t.Measure).HasColumnName("Measure");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
            Property(t => t.ItemTotalExcl).HasColumnName("ItemTotalExcl");
        }
    }
}
