using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class TimesheetMap : EntityTypeConfiguration<Timesheet>
    {
        public TimesheetMap()
        {
            // Primary Key
            HasKey(t => t.TimesheetsId);

            // Properties
            Property(t => t.Description)
                .IsRequired();

            // Table & Column Mappings
            ToTable("Timesheets");
            Property(t => t.TimesheetsId).HasColumnName("TimesheetsID");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
            Property(t => t.fkUsersID_Staff).HasColumnName("fkUsersID_Staff");
            Property(t => t.ForDate).HasColumnName("ForDate");
            Property(t => t.Hours).HasColumnName("Hours");
            Property(t => t.Description).HasColumnName("Description");
            Property(t => t.BillRate).HasColumnName("BillRate");
            Property(t => t.CostRate).HasColumnName("CostRate");
            Property(t => t.fkInvoicesID).HasColumnName("fkInvoicesID");
            Property(t => t.Status).HasColumnName("Status");
            Property(t => t.IsApproved).HasColumnName("IsApproved");
            Property(t => t.fkProjectActivitiesID).HasColumnName("fkProjectActivitiesID");
            Property(t => t.TimesheetGuid).HasColumnName("TimesheetGUID");

            // Relationships
            HasOptional(t => t.Invoice)
                .WithMany(t => t.Timesheets)
                .HasForeignKey(d => d.fkInvoicesID);
            HasOptional(t => t.ProjectActivity)
                .WithMany(t => t.Timesheets)
                .HasForeignKey(d => d.fkProjectActivitiesID);
            HasRequired(t => t.User)
                .WithMany(t => t.Timesheets)
                .HasForeignKey(d => d.fkUsersID_Staff);

        }
    }
}
