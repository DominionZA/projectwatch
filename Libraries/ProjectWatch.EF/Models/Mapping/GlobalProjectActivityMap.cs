using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace ProjectWatch.EF.Models.Mapping
{
    public class GlobalProjectActivityMap : EntityTypeConfiguration<GlobalProjectActivity>
    {
        public GlobalProjectActivityMap()
        {
            // Primary Key
            HasKey(t => t.GlobalProjectActivitiesId);

            // Properties
            Property(t => t.GlobalProjectActivityName)
                .IsRequired()
                .HasMaxLength(100);

            Property(t => t.GlobalProjectActivityDescription)
                .HasMaxLength(2000);

            // Table & Column Mappings
            ToTable("GlobalProjectActivities");
            Property(t => t.GlobalProjectActivitiesId).HasColumnName("GlobalProjectActivitiesID");
            Property(t => t.GlobalProjectActivityName).HasColumnName("GlobalProjectActivityName");
            Property(t => t.GlobalProjectActivityDescription).HasColumnName("GlobalProjectActivityDescription");
            Property(t => t.DateTimeInserted).HasColumnName("DateTimeInserted");
            Property(t => t.BillRate).HasColumnName("BillRate");
            Property(t => t.UseProjectBillRate).HasColumnName("UseProjectBillRate");
            Property(t => t.fkAccountsID).HasColumnName("fkAccountsID");
        }
    }
}
