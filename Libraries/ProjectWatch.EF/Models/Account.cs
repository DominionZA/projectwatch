using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public sealed class Account
    {
        public Account()
        {
            AccountLinkGateways = new List<AccountLinkGateway>();
            Invoices = new List<Invoice>();
            Users = new List<User>();
        }

        public int AccountsID { get; set; }
        public string AccountName { get; set; }
        public Guid? AccountSerial { get; set; }
        public DateTime? DateCreated { get; set; }
        public string AccountLogoPath { get; set; }
        public bool Active { get; set; }
        public string InvoiceNoPrefix { get; set; }
        public string InvoiceNoSuffix { get; set; }
        public int InvoiceNoSeed { get; set; }
        public int InvoiceNoLength { get; set; }
        public double InvoiceVATPercent { get; set; }
        public string PhysAddress1 { get; set; }
        public string PhysAddress2 { get; set; }
        public string PhysCity { get; set; }
        public string PhysProvince { get; set; }
        public string PhysCountry { get; set; }
        public string PhysPostalCode { get; set; }
        public string DayPhone { get; set; }
        public string MobilePhone { get; set; }
        public string FaxNo { get; set; }
        public string Email { get; set; }
        public string EmailSalutation { get; set; }
        public string DefaultTerms { get; set; }
        public string DefaultNotes { get; set; }
        public string VATNo { get; set; }
        public string CKNo { get; set; }
        public string HomePage { get; set; }
        public int? AccountLevel { get; set; }
        public int? fkPackagesID { get; set; }
        public Guid? ApplicationID { get; set; }
        public ICollection<AccountLinkGateway> AccountLinkGateways { get; set; }
        public ICollection<Invoice> Invoices { get; set; }
        public ICollection<User> Users { get; set; }
    }
}
