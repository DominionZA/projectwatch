namespace ProjectWatch.EF.Models
{
    public class InvoicePayment
    {
        public int InvoicePaymentsId { get; set; }
        public int fkInvoicesID { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public string PaymentNotes { get; set; }
        public decimal? PaymentAmount { get; set; }
        public int fkPaymentMethodsID { get; set; }
        public bool IsReversal { get; set; }
        public virtual Invoice Invoice { get; set; }
        public virtual PaymentMethod PaymentMethod { get; set; }
    }
}
