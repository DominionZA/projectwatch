namespace ProjectWatch.EF.Models
{
    public class Province
    {
        public int ProvincesId { get; set; }
        public string ProvinceName { get; set; }
    }
}
