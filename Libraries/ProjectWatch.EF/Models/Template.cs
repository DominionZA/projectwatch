namespace ProjectWatch.EF.Models
{
    public class Template
    {
        public int TemplatesId { get; set; }
        public string TemplateName { get; set; }
        public int fkAccountsID { get; set; }
        public bool IsPrimary { get; set; }
    }
}
