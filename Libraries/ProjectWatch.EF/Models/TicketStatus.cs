namespace ProjectWatch.EF.Models
{
    public class TicketStatus
    {
        public int TicketStatusId { get; set; }
        public string StatusCode { get; set; }
        public string StatusDescription { get; set; }
    }
}
