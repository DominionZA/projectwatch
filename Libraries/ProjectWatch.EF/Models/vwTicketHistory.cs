using System;

namespace ProjectWatch.EF.Models
{
    public class VwTicketHistory
    {
        public string FromUser { get; set; }
        public string FromAccountName { get; set; }
        public string FromEmail { get; set; }
        public int? FromAccountID { get; set; }
        public string ToAccount { get; set; }
        public int TicketHistoryID { get; set; }
        public int fkTicketsID { get; set; }
        public int fkUsersID { get; set; }
        public DateTime? DatePosted { get; set; }
        public string Body { get; set; }
    }
}
