using System;

namespace ProjectWatch.EF.Models
{
    public class TicketHistory
    {
        public int TicketHistoryId { get; set; }
        public int fkTicketsID { get; set; }
        public int fkUsersID { get; set; }
        public DateTime? DatePosted { get; set; }
        public string Body { get; set; }
    }
}
