using System;

namespace ProjectWatch.EF.Models
{
    public class Ticket
    {
        public int TicketsId { get; set; }
        public int fkUsersID { get; set; }
        public int fkAccountsID { get; set; }
        public DateTime? DatePosted { get; set; }
        public string Subject { get; set; }
        public string TicketNo { get; set; }
        public int? StatusCode { get; set; }
        public int? fkUsersID_Operator { get; set; }
        public Guid? GUID { get; set; }
    }
}
