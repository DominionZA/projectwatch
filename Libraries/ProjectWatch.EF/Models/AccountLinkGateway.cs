namespace ProjectWatch.EF.Models
{
    public class AccountLinkGateway
    {
        public int AccountLinkGatewayID { get; set; }
        public int fkAccountsID { get; set; }
        public int fkGatewaysID { get; set; }
        public bool Active { get; set; }
        public string MerchantNo { get; set; }
        public virtual Account Account { get; set; }
        public virtual Gateway Gateway { get; set; }
    }
}
