namespace ProjectWatch.EF.Models
{
    public class VwProduct
    {
        public string DisplayCpc { get; set; }
        public string DisplayCp { get; set; }
        public string Category { get; set; }
        public int? fkAccountsID { get; set; }
        public int ProductsId { get; set; }
        public int fkProductCategoriesID { get; set; }
        public string Description { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal CostPrice { get; set; }
        public string Code { get; set; }
        public double? OnHand { get; set; }
        public string Measure { get; set; }
    }
}
