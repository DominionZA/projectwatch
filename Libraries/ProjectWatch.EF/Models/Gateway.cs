using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public sealed class Gateway
    {
        public Gateway()
        {
            AccountLinkGateways = new List<AccountLinkGateway>();
        }

        public int GatewaysId { get; set; }
        public int Code { get; set; }
        public string Description { get; set; }
        public string URL { get; set; }
        public string Summary { get; set; }
        public ICollection<AccountLinkGateway> AccountLinkGateways { get; set; }
    }
}
