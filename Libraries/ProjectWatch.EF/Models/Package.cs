namespace ProjectWatch.EF.Models
{
    public class Package
    {
        public int PackagesId { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public int MaxCustomers { get; set; }
    }
}
