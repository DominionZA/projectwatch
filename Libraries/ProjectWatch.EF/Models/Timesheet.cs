using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public class Timesheet
    {
        public int TimesheetsId { get; set; }
        public DateTime? DateTimeInserted { get; set; }
        public int fkUsersID_Staff { get; set; }
        public System.DateTime ForDate { get; set; }
        public double Hours { get; set; }
        public string Description { get; set; }
        public decimal BillRate { get; set; }
        public decimal CostRate { get; set; }
        public int? fkInvoicesID { get; set; }
        public int? Status { get; set; }
        public bool? IsApproved { get; set; }
        public int? fkProjectActivitiesID { get; set; }
        public System.Guid TimesheetGuid { get; set; }
        public virtual Invoice Invoice { get; set; }
        public virtual ProjectActivity ProjectActivity { get; set; }
        public virtual User User { get; set; }
    }
}
