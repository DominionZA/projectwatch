using System.Data.Entity;
using ProjectWatch.EF.Models.Mapping;

namespace ProjectWatch.EF.Models
{
    public partial class SAV_ProjectWatchContext : DbContext
    {
        static SAV_ProjectWatchContext()
        {
            Database.SetInitializer<SAV_ProjectWatchContext>(null);
        }

        public SAV_ProjectWatchContext()
            : base("Data Source=41.185.13.201;Initial Catalog=SAV_ProjectWatch;Persist Security Info=True;User ID=msmit;Password=Sundio1")  //base("Name=SAV_ProjectWatchContext")
        {
        }

        public DbSet<C__ProjectActivitiesInProjects> C__ProjectActivitiesInProjects { get; set; }
        public DbSet<AccountLinkGateway> AccountLinkGateways { get; set; }
        public DbSet<Account> Accounts { get; set; }
        public DbSet<AccountUser> AccountUsers { get; set; }
        public DbSet<AddInUser> AddInUsers { get; set; }
        public DbSet<dtproperty> dtproperties { get; set; }
        public DbSet<Gateway> Gateways { get; set; }
        public DbSet<GlobalProjectActivity> GlobalProjectActivities { get; set; }
        public DbSet<InvoicePayment> InvoicePayments { get; set; }
        public DbSet<Invoice> Invoices { get; set; }
        public DbSet<Item> Items { get; set; }
        public DbSet<LineItem> LineItems { get; set; }
        public DbSet<Package> Packages { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<ProductCategory> ProductCategories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProjectActivity> ProjectActivities { get; set; }
        public DbSet<ProjectActivities_Default> ProjectActivities_Default { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Province> Provinces { get; set; }
        public DbSet<QuoteLineItem> QuoteLineItems { get; set; }
        public DbSet<Quote> Quotes { get; set; }
        public DbSet<SolutionsInProject> SolutionsInProjects { get; set; }
        public DbSet<SolutionTime> SolutionTimes { get; set; }
        public DbSet<Template> Templates { get; set; }
        public DbSet<TicketHistory> TicketHistories { get; set; }
        public DbSet<Ticket> Tickets { get; set; }
        public DbSet<TicketStatus> TicketStatus { get; set; }
        public DbSet<TimesheetBillingAndCostingRule> TimesheetBillingAndCostingRules { get; set; }
        public DbSet<Timesheet> Timesheets { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<UserProfile> UserProfiles { get; set; }
        public DbSet<VwAccount> vwAccounts { get; set; }
        public DbSet<VwBillingAndCostingRule> vwBillingAndCostingRules { get; set; }
        public DbSet<VwClientStatement> vwClientStatements { get; set; }
        public DbSet<VwDateTime> vwDateTimes { get; set; }
        public DbSet<VwInvoicePayment> vwInvoicePayments { get; set; }
        public DbSet<VwInvoice> vwInvoices { get; set; }
        public DbSet<VwInvoicesEx> vwInvoicesExes { get; set; }
        public DbSet<VwInvoiceSummaryByMonth> vwInvoiceSummaryByMonths { get; set; }
        public DbSet<VwProduct> vwProducts { get; set; }
        public DbSet<VwTicketHistory> vwTicketHistories { get; set; }
        public DbSet<VwTicket> vwTickets { get; set; }
        public DbSet<VwTimesheetBillingAndCostingRule> vwTimesheetBillingAndCostingRules { get; set; }
        public DbSet<VwTimesheet> vwTimesheets { get; set; }
        public DbSet<VwTimesheetsDaily> vwTimesheets_Daily { get; set; }
        public DbSet<VwUser> vwUsers { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new C__ProjectActivitiesInProjectsMap());
            modelBuilder.Configurations.Add(new AccountLinkGatewayMap());
            modelBuilder.Configurations.Add(new AccountMap());
            modelBuilder.Configurations.Add(new AccountUserMap());
            modelBuilder.Configurations.Add(new AddInUserMap());
            modelBuilder.Configurations.Add(new dtpropertyMap());
            modelBuilder.Configurations.Add(new GatewayMap());
            modelBuilder.Configurations.Add(new GlobalProjectActivityMap());
            modelBuilder.Configurations.Add(new InvoicePaymentMap());
            modelBuilder.Configurations.Add(new InvoiceMap());
            modelBuilder.Configurations.Add(new ItemMap());
            modelBuilder.Configurations.Add(new LineItemMap());
            modelBuilder.Configurations.Add(new PackageMap());
            modelBuilder.Configurations.Add(new PaymentMethodMap());
            modelBuilder.Configurations.Add(new ProductCategoryMap());
            modelBuilder.Configurations.Add(new ProductMap());
            modelBuilder.Configurations.Add(new ProjectActivityMap());
            modelBuilder.Configurations.Add(new ProjectActivities_DefaultMap());
            modelBuilder.Configurations.Add(new ProjectMap());
            modelBuilder.Configurations.Add(new ProvinceMap());
            modelBuilder.Configurations.Add(new QuoteLineItemMap());
            modelBuilder.Configurations.Add(new QuoteMap());
            modelBuilder.Configurations.Add(new SolutionsInProjectMap());
            modelBuilder.Configurations.Add(new SolutionTimeMap());
            modelBuilder.Configurations.Add(new TemplateMap());
            modelBuilder.Configurations.Add(new TicketHistoryMap());
            modelBuilder.Configurations.Add(new TicketMap());
            modelBuilder.Configurations.Add(new TicketStatusMap());
            modelBuilder.Configurations.Add(new TimesheetBillingAndCostingRuleMap());
            modelBuilder.Configurations.Add(new TimesheetMap());
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new VwAccountMap());
            modelBuilder.Configurations.Add(new VwBillingAndCostingRuleMap());
            modelBuilder.Configurations.Add(new VwClientStatementMap());
            modelBuilder.Configurations.Add(new VwDateTimeMap());
            modelBuilder.Configurations.Add(new VwInvoicePaymentMap());
            modelBuilder.Configurations.Add(new VwInvoiceMap());
            modelBuilder.Configurations.Add(new VwInvoicesExMap());
            modelBuilder.Configurations.Add(new VwInvoiceSummaryByMonthMap());
            modelBuilder.Configurations.Add(new VwProductMap());
            modelBuilder.Configurations.Add(new VwTicketHistoryMap());
            modelBuilder.Configurations.Add(new VwTicketMap());
            modelBuilder.Configurations.Add(new VwTimesheetBillingAndCostingRuleMap());
            modelBuilder.Configurations.Add(new VwTimesheetMap());
            modelBuilder.Configurations.Add(new VwTimesheetsDailyMap());
            modelBuilder.Configurations.Add(new VwUserMap());
        }
    }
}
