namespace ProjectWatch.EF.Models
{
    public class SolutionsInProject
    {
        public int SolutionsInProjectsId { get; set; }
        public int fkUsersID { get; set; }
        public int fkProjectsID { get; set; }
        public string SolutionFullName { get; set; }
    }
}
