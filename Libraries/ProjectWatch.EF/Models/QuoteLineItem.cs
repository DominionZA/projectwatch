using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public class QuoteLineItem
    {
        public int QuoteLineItemsId { get; set; }
        public int fkQuotesID { get; set; }
        public int? fkItemsID { get; set; }
        public string ItemCode { get; set; }
        public string ItemDescription { get; set; }
        public decimal? ItemPriceExcl { get; set; }
        public decimal? ItemCostExcl { get; set; }
        public decimal? ItemDiscount { get; set; }
        public double ItemQuantity { get; set; }
        public string Measure { get; set; }
        public DateTime? DateTimeInserted { get; set; }
        public double? ItemTotalExcl { get; set; }
    }
}
