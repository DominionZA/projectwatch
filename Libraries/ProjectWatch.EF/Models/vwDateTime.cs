using System;

namespace ProjectWatch.EF.Models
{
    public class VwDateTime
    {
        public DateTime CurrentDateTime { get; set; }
    }
}
