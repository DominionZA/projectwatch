namespace ProjectWatch.EF.Models
{
    public class VwClientStatement
    {
        public int InvoicesId { get; set; }
        public int fkUsersID { get; set; }
        public string InvoiceNo { get; set; }
        public System.DateTime ActionDate { get; set; }
        public decimal? Amount { get; set; }
        public decimal? DebitAmount { get; set; }
        public decimal? CreditAmount { get; set; }
        public int IsReversal { get; set; }
    }
}
