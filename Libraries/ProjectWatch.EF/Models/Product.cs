namespace ProjectWatch.EF.Models
{
    public class Product
    {
        public int ProductsId { get; set; }
        public int fkProductCategoriesID { get; set; }
        public string Description { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal CostPrice { get; set; }
        public string Code { get; set; }
        public double? OnHand { get; set; }
        public string Measure { get; set; }
    }
}
