namespace ProjectWatch.EF.Models
{
    public class VwInvoicePayment
    {
        public int InvoicePaymentsId { get; set; }
        public int fkInvoicesID { get; set; }
        public System.DateTime PaymentDate { get; set; }
        public string PaymentNotes { get; set; }
        public decimal? PaymentAmount { get; set; }
        public int fkPaymentMethodsID { get; set; }
        public int? DaysSinceDue { get; set; }
    }
}
