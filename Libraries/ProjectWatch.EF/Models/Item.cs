using System;
using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public class Item
    {
        public int ItemsId { get; set; }
        public string ItemDescription { get; set; }
        public decimal ItemCostIncl { get; set; }
        public decimal ItemPriceIncl { get; set; }
    }
}
