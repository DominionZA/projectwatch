using System;

namespace ProjectWatch.EF.Models
{
    public class VwTimesheet
    {
        public int TimesheetsId { get; set; }
        public DateTime? DateTimeInserted { get; set; }
        public int fkUsersID_Staff { get; set; }
        public int? fkProjectActivitiesID { get; set; }
        public System.DateTime ForDate { get; set; }
        public double Hours { get; set; }
        public string Description { get; set; }
        public bool? IsApproved { get; set; }
        public string ShortDescription { get; set; }
        public int? fkInvoicesID { get; set; }
        public int? Status { get; set; }
        public int? ProjectsId { get; set; }
        public int? AccountsId { get; set; }
        public int? fkUsersID_Client { get; set; }
        public string ProjectName { get; set; }
        public DateTime? ProjectStartDate { get; set; }
        public DateTime? ProjectEndDate { get; set; }
        public decimal? Budget { get; set; }
        public double? BudgetHours { get; set; }
        public string StaffFullName { get; set; }
        public string ProjectActivityName { get; set; }
        public decimal BillRate { get; set; }
        public decimal CostRate { get; set; }
        public double? TotalBilled { get; set; }
        public double? TotalCost { get; set; }
        public double? TotalProfit { get; set; }
        public string ClientCompanyName { get; set; }
        public string ShortClientCompanyName { get; set; }
        public string InvoiceNo { get; set; }
    }
}
