using System.Collections.Generic;

namespace ProjectWatch.EF.Models
{
    public sealed class PaymentMethod
    {
        public PaymentMethod()
        {
            InvoicePayments = new List<InvoicePayment>();
        }

        public int PaymentMethodsId { get; set; }
        public string Description { get; set; }
        public ICollection<InvoicePayment> InvoicePayments { get; set; }
    }
}
