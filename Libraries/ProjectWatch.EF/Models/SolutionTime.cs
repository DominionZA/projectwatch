namespace ProjectWatch.EF.Models
{
    public class SolutionTime
    {
        public int SolutionTimesId { get; set; }
        public int fkUsersID { get; set; }
        public string SolutionFullName { get; set; }
        public double? SessionHours { get; set; }
        public double TotalHours { get; set; }
    }
}
